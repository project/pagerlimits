<?php

/**
 * @file
 * Allows customizing the limits of your pagers.
 */

/**
 * Implements hook_permission().
 *
 * @return array
 */
function pagerlimits_permission() {
  return array(
    'administer pager limits' => array(
      'title' => t('Administer pager limits'),
      'description' => t('Allows setting a pager limit for every path that has a pager.'),
    ),
  );
}

/**
 * Implements hook_menu().
 *
 * @return array
 */
function pagerlimits_menu() {
  $items = array(
    'admin/config/user-interface/pagerlimits' => array(
      'title'            => 'Pager Limits',
      'description'      => 'Configure the pager limits for the detected pagers.',
      'file'             => 'pagerlimits.admin.inc',
      'page callback'    => 'drupal_get_form',
      'page arguments'   => array('pagerlimits_admin_form'),
      'access callback'  => 'user_access',
      'access arguments' => array('administer pager limits'),
  ));
  return $items;
}

/**
 * Implements hook_query_alter()
 *
 * @param QueryAlterableInterface $query
 */
function pagerlimits_query_pager_alter(QueryAlterableInterface $query) {
  static $invocation_count = 0;
  $limits = &drupal_static(__FUNCTION__);
  if (!isset($limits)) {
    $limits = variable_get('pagerlimits_limits', array());
  }

  $menu_item = menu_get_item();
  $path = $menu_item['path'];
  if (!empty($limits[$path][$invocation_count])) {
    $query->limit($limits[$path][$invocation_count]);
  }
  elseif (!isset($limits[$path][$invocation_count])) {
    $limits[$path][$invocation_count] = 0;
    variable_set('pagerlimits_limits', $limits);
  }
  $invocation_count++;
}

/**
 * Implements hook_theme().
 *
 * @return array
 */
function pagerlimits_theme() {
  return array(
    'pagerlimits_admin_table' => array(
      'render element' => 'form',
      'file' => 'pagerlimits.admin.inc',
    ),
  );
}
