
README.txt for Pager Limits 7.x-1.x


This module collects pagers as you visit pages that have them, and then it
allows you to configure the limits of each pager.

The 'Administer pager limits' determines who can set the limits at
admin/config/user-interface/pagerlimits.
