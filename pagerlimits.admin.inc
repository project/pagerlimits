<?php

/**
 * @file
 * Allows customizing the limits of your pagers.
 */

/**
 * Form builder: Builds the Pager Limits administration form.
 *
 * Allows the administrator to set a limit for every known pager.
 *
 * @param $form
 * @param $form_state
 *
 * @return array
 */
function pagerlimits_admin_form($form, &$form_state) {
  $limits = variable_get('pagerlimits_limits', array());
  ksort($limits);
  if (empty($limits)) {
    $form['info'] = array(
      '#markup' => t('No pagers found yet.'),
    );
  }
  else {
    $form['limits']['#theme'] = 'pagerlimits_admin_table';
    foreach ($limits as $path => $limit) {
      if (count($limit) == 1) {
        $form['limits'][] = array(
          '#type'          => 'textfield',
          '#title'         => $path,
          '#default_value' => (empty($limit[0]) ? '' : $limit[0]),
          '#size'          => 3,
          '#maxlength'     => 8,
        );
      }
      else {
        foreach ($limit as $i => $l) {
          $form['limits'][] = array(
            '#type'          => 'textfield',
            '#title'         => $path . "[$i]",
            '#default_value' => (empty($limit[$i]) ? '' : $limit[$i]),
            '#size'          => 3,
            '#maxlength'     => 8,
          );
        }
      }
    }
    $form['desc'] = array(
      '#type'        => 'item',
      '#description' => t('To restore a default pager limit, just clear the corresponding input field.'),
    );
    $form['submit'] = array(
      '#type'  => 'submit',
      '#value' => t('Save configuration')
    );
  }
  return $form;
}

/**
 * Submit handler for the pagerlimits_admin_form.
 *
 * @param $form
 * @param $form_state
 */
function pagerlimits_admin_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $limits = array();
  foreach (element_children($form['limits']) as $key) {
    list($path, $index) = explode('[', $form['limits'][$key]['#title'] . '[0]');
    $value = trim($values[$key]);
    if (!empty($value)) {
      $limits[$path][(int) $index] = $value;
    }
  }
  variable_set('pagerlimits_limits', $limits);
}

/**
 * Theme function for the pagerlimits_admin_form.
 *
 * @param $variables
 *
 * @return string
 */
function theme_pagerlimits_admin_table($variables) {
  $form = $variables['form'];
  $header = array(
    t('Path'),
    t('Pager Limit'),
  );

  $row = array();
  foreach (element_children($form) as $key) {
    $form[$key]['#title_display'] = 'invisible';
    $form[$key]['#prefix'] = '<div style="float: left;">';
    $form[$key]['#suffix'] = '</div>';
    $path = $form[$key]['#title'];
    if ($pos = strpos($path, '[')) {
      $path = substr($path, 0, $pos);
      if (isset($row[0])) {
        if ($row[0] == $path) {
          $row[1]['data'] .= drupal_render($form[$key]);
          continue;
        }
      }
    }
    if (!empty($row)) {
      $rows[] = $row;
      $row = array();
    }
    $row[] = $path;
    $row[] = array('data' => drupal_render($form[$key]));
  }
  $rows[] = $row;

  $output = theme('table', array(
    'header'     => $header,
    'rows'       => $rows,
    'attributes' => array('id' => 'pagerlimits-limits', 'style' => array('width: auto;'))
  ));
  $output .= drupal_render_children($form);
  return $output;
}
